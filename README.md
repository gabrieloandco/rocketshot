# Rocketshot
Deployment of the rocketshot app.

Dependencies:
- kubectl
- helm
- python3

## Create Local Kubernetes Cluster (if you dont have one)
Dependencies:
- virtualbox- 
- ansible 
- vagrant 

Set the Environment:

To do this you have to modify MASTER and NODES variables in the vagrant file to specify how many masters and nodes you want to be deployed. After that you have to modify the ansible inventory (./ansible/hosts.ini) to have the new nodes and masters that will be 

Create the clusters:

Just run: vagrant up

Check that the clusters are running properly:

vagrant ssh master1
sudo -i
export KUBECONFIG=/etc/kubernetes/admin.conf &&  kubectl get nodes

vagrant ssh master2
sudo -i
export KUBECONFIG=/etc/kubernetes/admin.conf &&  kubectl get nodes

Use the cluster:
export KUBECONFIG=./ansible/.kube/config && kubectl get nodes

## Deploy the kubernetes resources
kubectl apply -f kubernetes/resources/namespace.yaml
kubectl apply -f kubernetes/resources

## Test the deployment generator for event channels
python channel_generator.py


