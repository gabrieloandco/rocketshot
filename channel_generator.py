import argparse
import uuid
from subprocess import run

parser=argparse.ArgumentParser(description="Deployment generation parameters")
parser.add_argument("--id")
args = parser.parse_args()

if args.id:
    channel_id=args.id
else:
    channel_id=str(uuid.uuid1())
script = 'export KUBECONFIG=./ansible/.kube/config && \
helm install -n rocketshot eventchannel-{id} --set id={id} ./kubernetes/eventchannels/'.format(id=channel_id)
print(script)
run(script, shell=True)
print("Chart eventchannel-{id} Created".format(id=channel_id))
print("Uninstall with:  helm uninstall -n rocketshot eventchannel-{id}".format(id=channel_id))